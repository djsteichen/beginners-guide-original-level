﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ModelSwitchScript : MonoBehaviour
{
    public GameObject modelON;
    public GameObject modelOFF;

    private int modelNum;

    void Start()
    {
        modelNum = 2;
        modelON.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        /*if (Input.GetKeyDown(KeyCode.Return))
        {
            SwitchModels();
        }*/
    }

    public void SwitchModels()
    {
        if (modelNum == 1)
        {
            modelON.SetActive(false);
            modelOFF.SetActive(true); //Lamp OFF
            modelNum = 2;
        }
        else if (modelNum == 2)
        {
            modelON.SetActive(true);
            modelOFF.SetActive(false); //Lamp ON
            modelNum = 1;
        }
    }
    public void Test()
    {
        Debug.Log("ModelSwitchScriptAccessed");
    }
}
