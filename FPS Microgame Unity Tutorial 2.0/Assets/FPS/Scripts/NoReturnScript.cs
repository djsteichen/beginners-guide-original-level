﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class NoReturnScript : MonoBehaviour
{
    public GameObject hiddenEntrywayWall;

    private AudioSource doorAudioSource;
    private AudioClip doorTriggerSFX;
    public AudioClip[] doorOpenCloseSFX;

    [SerializeField]
    private bool playerEntered = false; 

    private void Start()
    {
        doorAudioSource = this.GetComponent<AudioSource>();
        doorOpenCloseSFX = new AudioClip[]
        {
            (AudioClip)Resources.Load("DoorOpenClose")
        }; //populate the array with specific audioclips

    }
    private void OnTriggerEnter(Collider other)
    {
        if (playerEntered == false)
        {
            int index = Random.Range(0, doorOpenCloseSFX.Length);
            doorTriggerSFX = doorOpenCloseSFX[index];
            doorAudioSource.clip = doorTriggerSFX;
            doorAudioSource.Play();

            hiddenEntrywayWall.SetActive(true); //shut the door behind you;
            Camera.main.DOShakePosition(2, 0.3f, 40, 90, true);
            playerEntered = true;
        }
        

        

    }

}
