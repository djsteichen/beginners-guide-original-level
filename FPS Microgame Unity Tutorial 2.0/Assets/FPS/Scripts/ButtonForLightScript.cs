﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class ButtonForLightScript : MonoBehaviour
{
    public ModelSwitchScript modelSwitchScriptReference;
    public NarrationScript narrationScriptReference;

    public float secondsBetweenVocalCues = 7f; //how long before cues are played and light turns back on and button is pressable
    public float currentTime = 0f;

    public float secondsToWin = 25f;
    public float countdownTimeToWin = 0f;

    public float narrationDelay = 3f;
    public float countdownTimeToNarration = 0f;

    public bool clickable = true;
    public bool playerReadyUp = false;
    public bool toxicPromptPlaying = false;
    public bool daveyIntroNarrationPlayed = false;
    public bool playerCanPressEnter = false;
    public bool narrationNeedsToPlay = false;
    public bool finalNarrationPlayed = false;
    private bool playerInputBuffer = false;

    public GameObject PromptLight;
    private AudioClip promptAudio;
    private AudioSource narrationAudioSource;
    private int vocalCueCounter;
    private int toxicArrayIndex;
    private int normalArrayIndex;
    private int narrationTimerIndex = 1;

    private AudioSource doorAudioSource;
    private AudioClip doorTriggerSFX;
    public AudioClip[] doorOpenCloseSFX;

    public bool isAudioClear = false; //changes when player presses a button (after Davey's narration)
    public AudioClip[] muffledAudioClips; //array to hold muffled vocal cues
    public AudioClip[] clearToxicAudioclips; //array to hold clear vocal cues
    public AudioClip[] clearNormalAudioclips;
    //https://answers.unity.com/questions/668721/how-do-i-assign-audio-files-to-an-audioclip-array.html
    private void Start()
    {
        //set the timer value
        currentTime = secondsBetweenVocalCues;

        //populate garbled narration array
        narrationAudioSource = this.GetComponent<AudioSource>();
        muffledAudioClips = new AudioClip[] 
        {
            (AudioClip)Resources.Load("GarbledAudio0"),
            (AudioClip)Resources.Load("GarbledAudio1"),
            (AudioClip)Resources.Load("GarbledAudio2"),
            (AudioClip)Resources.Load("GarbledAudio3")
        }; //populate the array with specific audioclips

        //populate clear toxic vocal cue array
        narrationAudioSource = this.GetComponent<AudioSource>();
        clearToxicAudioclips = new AudioClip[]
        {
            (AudioClip)Resources.Load("DaveToxic0"),
            (AudioClip)Resources.Load("SamToxic0"),
            (AudioClip)Resources.Load("DaveToxic2"),
            (AudioClip)Resources.Load("SamToxic2"),
            (AudioClip)Resources.Load("DaveToxic4")
        }; //populate the array with specific audioclips

        narrationAudioSource = this.GetComponent<AudioSource>();
        clearNormalAudioclips = new AudioClip[]
        {
            
            (AudioClip)Resources.Load("DaveNormalEat"),
            (AudioClip)Resources.Load("SamNormalBFF"),
            (AudioClip)Resources.Load("DaveNormalInternetFame"),
            (AudioClip)Resources.Load("SamNormalImmortalDog"),
            (AudioClip)Resources.Load("DaveNormalParadise"),
            (AudioClip)Resources.Load("SamNormalImportantToOthers"),
            (AudioClip)Resources.Load("DaveNormalRememberedForever"),
            (AudioClip)Resources.Load("SamNormalLifeOfParty"),
            (AudioClip)Resources.Load("DaveNormalRich"),
            (AudioClip)Resources.Load("SamNormalNiceCar"),
            (AudioClip)Resources.Load("DaveNormalFeelSuccessful"),
            (AudioClip)Resources.Load("SamNormalAttractive")
        }; //populate the array with specific audioclips

        //populate doorOpenCloseSFX array
        doorAudioSource = this.GetComponent<AudioSource>();
        doorOpenCloseSFX = new AudioClip[]
        {
            (AudioClip)Resources.Load("DoorOpenClose")
        };

    }

    public void PlayVocalPromptAudio(int randNum)
    {
        if (isAudioClear == false)
        {
            //muffledAudioclips array | plays a random sound from the garbled audio array when player enters collider
            Debug.Log("PlayVocalPromptAudio() called");
            int index = Random.Range(0, muffledAudioClips.Length);
            promptAudio = muffledAudioClips[index];
            narrationAudioSource.clip = promptAudio;
            narrationAudioSource.Play();
        }

        if (isAudioClear == true)
        {
            if (randNum >=7) //pull from toxic clear audio array
            {
                Debug.Log("toxic prompt should be playing. randNum = " + randNum);
                toxicPromptPlaying = true;
                //int index = 0; //Random.Range(0, clearToxicAudioclips.Length);
                promptAudio = clearToxicAudioclips[toxicArrayIndex];
                narrationAudioSource.clip = promptAudio;
                narrationAudioSource.Play();

                toxicArrayIndex++;
                Debug.Log("toxicArrayIndex is now " + toxicArrayIndex);
                if (toxicArrayIndex> clearToxicAudioclips.Length-1)
                {
                    toxicArrayIndex = 0; //reset index val once it goes all the way through
                    Debug.Log("toxic array index reset to " + toxicArrayIndex);
                }

                countdownTimeToWin = secondsToWin; //set the timer for the Win Timer
                
                //make and start END/WIN STATE timer here
                //if timer reaches zero, open up secret door 
                //(you'll have to change OpenHiddenDoor() code below)
            }
            else //pull from normal clear audio array
            {
                Debug.Log("normal prompt should be playing. randNum = " + randNum);
                toxicPromptPlaying = false;
                //int index = Random.Range(0, clearNormalAudioclips.Length);
                promptAudio = clearNormalAudioclips[normalArrayIndex];
                narrationAudioSource.clip = promptAudio;
                narrationAudioSource.Play();
                normalArrayIndex++;
                Debug.Log("normalArrayIndex is now " + normalArrayIndex);

                if (normalArrayIndex>clearNormalAudioclips.Length-1)
                {
                    normalArrayIndex = 0;
                    Debug.Log("normal array index reset to " + normalArrayIndex);

                }
            }
            
            
        }
        
    }
    private void FixedUpdate()
    {
        TurnOffLight();
        
    }
    private void Update()
    {
        ReadyUp();
        Timer();
        StartWinTimer();
        if (playerCanPressEnter && Input.GetKeyDown(KeyCode.Return))
        {
            isAudioClear = true;
            Debug.Log("Vocal Audio Cleared Up");
        }

        if (narrationNeedsToPlay)
        {
            StartNarrationTimer(narrationTimerIndex);
        }
        if(narrationScriptReference.daveyAudioSource.isPlaying == false)
        {
            playerInputBuffer = false;
        }
    }

    void Timer()
    {
        //timer code
        currentTime -= 1f * Time.deltaTime;
        //Debug.Log(currentTime);

        if (currentTime <=0 && playerReadyUp == true && !PromptLight.activeInHierarchy) /*&& clickable == true*/
        {
            
            currentTime = secondsBetweenVocalCues; //reset timer value

            //code below only runs if button + light are off
            if (clickable != true) //reset button position and turn on light
            {
                PlayVocalPromptAudio(Random.Range(0,10)); //play audio vocal cue (either garbled or clear)
                clickable = true;
                Debug.Log("clickable variable set to true");
                ResetButton(); //move button back to "pressable" state
                Debug.Log("button position reset");
                TurnOnLight();
                print("TurnOnLight() called");

            }
        }
    }

    void StartWinTimer()
    {
        if (toxicPromptPlaying ==true)
        {
            countdownTimeToWin -= 1f * Time.deltaTime; //countdown 1 second until 0
            Debug.Log(countdownTimeToWin);
        }

        if (countdownTimeToWin <0)
        {
            OpenHiddenDoor(); //open secret door to lamppost
        }
        

    }

    void StartNarrationTimer(int narrationIndex)
    {
        countdownTimeToNarration -= 1f * Time.deltaTime;
        Debug.Log("countdownTimeToNarration = " + countdownTimeToNarration);

        if (countdownTimeToNarration<=0)
        {
            narrationScriptReference.PlayDaveyNarration(narrationIndex);
            if (narrationScriptReference.daveyAudioSource.isPlaying ==true)
            {
                playerInputBuffer = true;
            }
            narrationNeedsToPlay = false;
            narrationTimerIndex++; //increase to 2
        }
    }

    public void TurnOnLight()
    {
        vocalCueCounter += 1;
        Debug.Log("vocalCueCounter is now " + vocalCueCounter);
        PromptLight.SetActive(true); //turns on 'PromptLight' game obj
        modelSwitchScriptReference.SwitchModels(); //change Lamp model from OFF to ON state

        
        if (vocalCueCounter == 5)
        {
            //need a simple timer here also, so that it doesn't play on top of something else
            countdownTimeToNarration = narrationDelay; //set the timer
            narrationNeedsToPlay = true; //triggers the update function to fire
            //StartNarrationTimer(narrationTimerIndex); //should be a val of 1 here
            //Davey tells player to press Enter for clear audio
            playerCanPressEnter = true;

        }
        else if (vocalCueCounter == 10)
        {
            countdownTimeToNarration = narrationDelay; //set the timer
            narrationNeedsToPlay = true; //triggers the update function to fire
            finalNarrationPlayed = true;
            //narrationTimerIndex++; 
        }
        else
        {
            Debug.Log("no narration played. vocalCueCounter is " + vocalCueCounter);
        }
    }

    void OnTriggerEnter()
    {
        if (vocalCueCounter == 0 && daveyIntroNarrationPlayed == false)
        {
            narrationScriptReference.PlayDaveyNarration(0); //(INTRO) press R to begin dialogue
            daveyIntroNarrationPlayed = true; //so the player can't trigger this audio cue twice
        }
    }

    public void TurnOffLight()
    {
        //if this.clickable == true && Input.GetMouseDown, turn off light
        if (clickable == true && playerInputBuffer == false
            && Input.GetMouseButton(0)
            && GameObject.Find("PromptLight") != null
            && finalNarrationPlayed == false) //LMB click
            if(GameObject.Find("PromptLight").activeInHierarchy == true)
            {
                toxicPromptPlaying = false; //so you don't accidentally open the hidden door
                modelSwitchScriptReference.SwitchModels(); //change model from ON to OFF state
                PressButton(); //physically depresses button in worldspace
                GameObject.Find("PromptLight").SetActive(false); //turns off 'PromptLight' game obj
                clickable = false;
                Debug.Log("TurnOffLight() executed");
                currentTime = secondsBetweenVocalCues;
                Debug.Log("Timer val reset to " + secondsBetweenVocalCues);
                
            }
    }

    public void PressButton()
    {
        this.gameObject.transform.position = new Vector3(-31.1f, 1.3f, -1.5f); 
    }

    public void ResetButton()
    {
        this.gameObject.transform.position = new Vector3(-31.0f, 1.3f, -1.5f);
    }

    public void OpenHiddenDoor()
    {
        if (GameObject.Find("HiddenExitDoor") != null)
        {
            //shake that camera bb
            Camera.main.DOShakePosition(2, 0.3f, 40, 90, true);

            //play doorOpenCloseSFX
            doorTriggerSFX = doorOpenCloseSFX[0];
            doorAudioSource.clip = doorTriggerSFX;
            doorAudioSource.Play();

            Debug.Log("hiddenExitDoor function called");
            GameObject.Find("HiddenExitDoor").SetActive(false);

            if (finalNarrationPlayed == false)
            {
                finalNarrationPlayed = true;
                narrationScriptReference.PlayDaveyNarration(2);
                playerInputBuffer = true;
                //play final narration if player reaches the end and it hasn't played
            }
        }
        
    }
    void ReadyUp()
    {
        if (Input.GetKeyDown(KeyCode.R))
        {
            Debug.Log("Player ready up-ed");
            playerReadyUp = true;
            //for some reason, the door collider disappears but not the mesh?
        }
    }
}
