﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class AutoStartScript : MonoBehaviour
{
    private float countdownToStart;
    private float secondsToStart = 3f;
    // Start is called before the first frame update
    void Start()
    {
        countdownToStart = secondsToStart;
    }

    // Update is called once per frame
    void Update()
    {
        Debug.Log("countdownToStart = " + countdownToStart);
        countdownToStart -= 1f * Time.deltaTime;
        if (countdownToStart <=0)
        {
            SceneManager.LoadScene(sceneName: "MainScene");
        }
    }
}
