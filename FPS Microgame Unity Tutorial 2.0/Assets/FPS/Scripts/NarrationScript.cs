﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NarrationScript : MonoBehaviour
{
    public AudioSource daveyAudioSource; //for narration
    private AudioClip daveyAudio;
    public AudioClip[] daveyNarrationArray;
    // Start is called before the first frame update
    void Start()
    {
        daveyAudioSource = this.GetComponent<AudioSource>();
        daveyNarrationArray = new AudioClip[]
        {
            (AudioClip)Resources.Load("DaveyNarration0"),
            (AudioClip)Resources.Load("DaveyNarration1"),
            (AudioClip)Resources.Load("DaveyNarration2")

        }; //populate the array with specific audioclips
    }

    public void PlayDaveyNarration(int index)
    {
        Debug.Log("PlayDaveyNarration() called");
        daveyAudio = daveyNarrationArray[index];
        daveyAudioSource.clip = daveyAudio;
        daveyAudioSource.Play();


    }
}
